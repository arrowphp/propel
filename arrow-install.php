<?php

declare(strict_types=1);

use Arrow\Application;
use Arrow\Config;
use Arrow\PackageInterface;

return new class implements PackageInterface {
	public function init(Application $app): void {
		$app->get(Config::class)->add('Middleware', \Arrow\Propel\Middleware::class);
	}
};
