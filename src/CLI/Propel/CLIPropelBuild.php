<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI\Propel;

use Propel\Generator\Application;

class CLIPropelBuild extends CLIPropel {


	protected function executeAction(): void {
		$app = $this->createPropelApplication();
		$this->runPropelCommand($app, [
			'build',
			'--config-dir' => $this->pathRoot,
			'--schema-dir' => $this->pathRoot,
			'--output-dir' => $this->pathModels,
		]);
	}

	protected function showHelp(string $error = null): void {
		echo "Build the Propel model classes based on the schema.\n";
		echo "Usage: propel:build [arguments]\n";

		if ($error) {
			echo "  Error: {$error}\n";
		}

		echo "\nArguments:\n";
		echo "  --help      Shows this help.\n";

		echo "\nExample:\n";
		echo "> propel:build\n";
	}

	protected function createPropelApplication(): Application {
		$app = parent::createPropelApplication();

		$app->add(new \Propel\Generator\Command\ModelBuildCommand());

		return $app;
	}
}
