<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI\Propel;

use Propel\Generator\Application;

class CLIPropelMigrate extends CLIPropel {

	public const ACTION_UP = 'up';
	public const ACTION_DOWN = 'down';
	public const ACTION_ALL = 'all';

	private $action;

	private $map = [
		self::ACTION_UP => ['up', \Propel\Generator\Command\MigrationUpCommand::class],
		self::ACTION_DOWN => ['down', \Propel\Generator\Command\MigrationDownCommand::class],
		self::ACTION_ALL => ['migrate', \Propel\Generator\Command\MigrationMigrateCommand::class],
	];

	public function __invoke($options): void {
		$this->action = $this->getAction($options);
		parent::__invoke($options);
	}

	protected function executeAction(): void {
		if (!in_array($this->action, [self::ACTION_UP, self::ACTION_DOWN, self::ACTION_ALL])) {
			$this->showHelp("No action specified.");
			return;
		}

		list($key, $class) = $this->map[$this->action];

		$app = $this->createPropelApplication();

		$app->add(new $class());

		$this->runPropelCommand($app, [
			$key,
			'--config-dir' => $this->pathRoot,
			'--output-dir' => $this->pathMigrations,
		]);
	}

	protected function showHelp(string $error = null): void {
		echo "Run a migration, or all migrations.\n";
		echo "Usage: propel:migrate [action:up|down|all] [arguments]\n";

		if ($error) {
			echo "  Error: {$error}\n";
		}

		echo "\nArguments:\n";
		echo "  --help      Shows this help.\n";

		echo "\nExample:\n";
		echo "> propel:migrate all\n";
	}

	private function getAction(array $options): ?string {
		if (empty($options['words']['0'])) {
			return null;
		}

		return strtolower(trim($options['words']['0']));
	}
}
