<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI\Propel;

use Arrow\Config;
use League\Container\Container;
use Propel\Runtime\Propel;
use Propel\Generator\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Arrow\Helper;

abstract class CLIPropel {


	/**
	 * @var \Arrow\Config
	 */
	protected $config;
	protected $container;

	protected $pathRoot;
	protected $pathModels;
	protected $pathMigrations;

	public function __construct(Container $container, Config $config) {
		$this->container = $container;
		$this->config = $config;

		$path = (string)$this->config->get('Path');
		$this->pathRoot = $path . '/orm/';
		$this->pathModels = $this->pathRoot . '/models/';
		$this->pathMigrations = $path . '/migrations/';
	}

	public function __invoke($options): void {
		if (isset($options['args']['help'])) {
			$this->showHelp();
			return;
		} elseif (!$this->hasPropelConnections()) {
			$this->showHelp('No Propel Connection(s) defined.');
			return;
		}

		$this->setupDirectories();
		$this->updatePropelProperties();

		$this->executeAction();
	}

	abstract protected function executeAction(): void;

	protected function hasPropelConnections(): bool {
		return !empty($this->config->get('package.arrowphp/propel.Propel.database.connections'));
	}

	protected function setupDirectories(): void {
		foreach ([$this->pathRoot, $this->pathModels, $this->pathMigrations] as $path) {
			if (!is_dir($path)) {
				mkdir($path);
			}
		}
	}

	protected function updatePropelProperties(): void {
		$properties = $this->config->get('package.arrowphp/propel.Propel');
		$result = Helper::var_export_pretty(['propel' => $properties], true);
		file_put_contents($this->pathRoot . '/propel.php', "<?php \n\n/* GENERATED - DO NOT EDIT */\n\nreturn {$result};");
	}

	abstract protected function showHelp(string $error = null): void;

	protected function createPropelApplication(): Application {
		$app = new Application('Propel', Propel::VERSION);
		$app->setAutoExit(false);

		return $app;
	}

	protected function runPropelCommand(Application $app, array $args): void {
		$app->run(new ArrayInput($args), new \Symfony\Component\Console\Output\StreamOutput(fopen('php://output', 'w')));
	}
}
