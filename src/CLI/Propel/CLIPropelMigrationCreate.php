<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI\Propel;

use Propel\Generator\Application;

class CLIPropelMigrationCreate extends CLIPropel {


	private $name;

	public function __invoke($options): void {
		$this->name = preg_replace('/[^a-z0-9\-]+/', '_', trim(implode(" ", $options['words'])));
		parent::__invoke($options);
	}

	protected function executeAction(): void {
		if (!$this->name) {
			$this->showHelp("No name specified.");
			return;
		}

		$app = $this->createPropelApplication();
		$this->runPropelCommand($app, [
			'create',
			'--config-dir' => $this->pathRoot,
			'--schema-dir' => $this->pathRoot,
			'--output-dir' => $this->pathMigrations,
			'--suffix' => $this->name,
		]);
	}

	protected function showHelp(string $error = null): void {
		echo "Create an empty migration.\n";
		echo "Usage: propel:migration:create name [arguments]\n";

		if ($error) {
			echo "  Error: {$error}\n";
		}

		echo "\nArguments:\n";
		echo "  --help      Shows this help.\n";

		echo "\nExample:\n";
		echo "> propel:migration:create Session table table\n";
	}

	protected function createPropelApplication(): Application {
		$app = parent::createPropelApplication();

		$app->add(new \Propel\Generator\Command\MigrationCreateCommand());

		return $app;
	}
}
