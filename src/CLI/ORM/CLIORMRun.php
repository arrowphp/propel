<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI\ORM;

use Arrow\Config;
use League\Container\Container;
use Propel\Runtime\Propel;
use Propel\Generator\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Arrow\Helper;

class CLIORMRun {


	/**
	 * @var \Arrow\Config
	 */
	private $config;
	private $container;
	private $pdo;

	public function __construct(Container $container, Config $config) {
		$this->container = $container;
		$this->config = $config;
	}

	public function __invoke($options) {
		if (isset($options['args']['help'])) {
			$this->showHelp();
			return;
		}

		// try {
		//     $this->pdo = $this->container->get(App::CONTAINER_DATABASE_DEFAULT);
		// } catch (NotFoundException $e) {
		//     $this->showHelp('No default connection available.');
		//     return;
		// }

		// $dryRun = isset($options['args']['dry-run']);
		$verbose = isset($options['args']['verbose']);

		$pathRoot = (string)$this->config->get('Path') . '/orm/';
		$pathFiles = $pathRoot . '/models/';

		$phpNameFix = $this->config->get('package.arrowphp/propel.PHPNameFix');
		$phpNameFixIndex = array_combine(
			array_map('strtolower', $phpNameFix),
			$phpNameFix
		);

		$properties = $this->config->get('package.arrowphp/propel.Propel');

		if (empty($properties['database']['connections'])) {
			$this->showHelp('No Propel Connection(s) defined.');
			return;
		}

		foreach ([$pathRoot, $pathFiles] as $path) {
			if (!is_dir($path)) {
				mkdir($path);
			}
		}

		$result = Helper::var_export_pretty(['propel' => $properties], true);
		file_put_contents($pathRoot . '/propel.php', "<?php \n\n/* GENERATED - DO NOT EDIT */\n\nreturn {$result};");

		$app = $this->createPropelGeneratorApplication();
		$app->setAutoExit(false);

		$output = new \Symfony\Component\Console\Output\ConsoleOutput();

		$app->run(new ArrayInput([
			'reverse',
			'--config-dir' => $pathRoot,
			'--namespace' => '\\Model',
			'--output-dir' => $pathRoot,
		]), $output);

		$schemaFile = $pathRoot . '/schema.xml';
		$schema = file_get_contents($schemaFile);

		$xml = new \SimpleXMLElement($schema);
		foreach ($xml->table as $table) {
			$tableNameLower = strtolower((string)$table['phpName']);
			if (isset($phpNameFixIndex[$tableNameLower])) {
				$table['phpName'] = $phpNameFixIndex[$tableNameLower];
			}

			/* @phan-suppress-next-line PhanTypeExpectedObjectPropAccess */
			foreach ($table->column as $column) {
				$columnNameLower = strtolower((string)$column['phpName']);
				if (isset($phpNameFixIndex[$columnNameLower])) {
					// echo "Update {$table['phpName']}.{$columnNameLower} to {$phpNameFixIndex[$columnNameLower]}\n";
					$column['phpName'] = $phpNameFixIndex[$columnNameLower];
				}

				if ($column['defaultValue'] == 'CURRENT_TIMESTAMP') {
					$column['defaultExpr'] = 'CURRENT_TIMESTAMP';
					unset($column['defaultValue']);
				}
			}
		}

		file_put_contents($schemaFile, $xml->asXML());

		$app->run(new ArrayInput([
			'build',
			'--config-dir' => $pathRoot,
			'--schema-dir' => $pathRoot,
			'--output-dir' => $pathFiles,
		]), $output);

		$app->setAutoExit(false);
	}

	private function showHelp($error = null) {
		echo "Run the DB migrations.\n";
		echo "Usage: orm:run [arguments]\n";

		if ($error) {
			echo "  Error: {$error}\n";
		}

		echo "\nArguments:\n";
		echo "  --help      Shows this help.\n";
		echo "  --dry-run   Print the names of the migrations that will be run.\n";

		echo "\nExample:\n";
		echo "> orm:run\n";
	}

	private function createPropelGeneratorApplication() {
		$app = new Application('Propel', Propel::VERSION);

		// These are the only commands we need to run.
		$app->add(new \Propel\Generator\Command\DatabaseReverseCommand());
		$app->add(new \Propel\Generator\Command\ModelBuildCommand());

		return $app;
	}
}
