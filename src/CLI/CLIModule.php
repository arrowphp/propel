<?php

declare(strict_types=1);

namespace Arrow\Propel\CLI;

use Arrow\Config;
use Arrow\CLI\CLI;
use Arrow\CLI\ModuleInterface as ModuleCLIInterface;
use League\Container\Container;

class CLIModule implements ModuleCLIInterface {

	public function registerCli(CLI $cli, Container $container, Config $config): void {
		$cli->register('propel:build', 'Build the Propel model classes based on the schema.', 'cli.propel.build');
		$cli->register('propel:migration:create', 'Create an empty migration.', 'cli.propel.migrationcreate');
		$cli->register('propel:migration:diff', 'Create a migration based on the difference between the schema and database.', 'cli.propel.migrationdiff');
		$cli->register('propel:migrate', 'Run a migration, or all migrations.', 'cli.propel.migrate');
	}

	public function registerServices(Container $container, Config $config): void {
		$container->add('cli.propel.build', Propel\CLIPropelBuild::class)
			->addArguments([$container, $config]);
		$container->add('cli.propel.migrationdiff', Propel\CLIPropelMigrationDiff::class)
			->addArguments([$container, $config]);
		$container->add('cli.propel.migrationcreate', Propel\CLIPropelMigrationCreate::class)
			->addArguments([$container, $config]);
		$container->add('cli.propel.migrate', Propel\CLIPropelMigrate::class)
			->addArguments([$container, $config]);
	}
}
