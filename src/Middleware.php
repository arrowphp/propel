<?php

declare(strict_types=1);

namespace Arrow\Propel;

use Arrow\Application;
use Arrow\Config;
use Arrow\MiddlewareInterface;
use Propel\Runtime\Propel;
use Propel\Runtime\Connection\ConnectionManagerSingle;
use Propel\Runtime\ServiceContainer\StandardServiceContainer;

class Middleware implements MiddlewareInterface {


	public function process(Application $app): void {
		$this->setupPropelConnection($app);
	}

	private function setupPropelConnection(Application $app): void {
		$configFile = (string)$app->get(Config::class)->get('Path.Base') . '/orm/propel.php';

		if (!file_exists($configFile)) {
			return;
		}

		$config = require $configFile;

		$serviceContainer = Propel::getServiceContainer();
		assert($serviceContainer instanceof StandardServiceContainer);

		foreach ($config['propel']['database']['connections'] as $name => $connection) {
			$serviceContainer->setAdapterClass($name, $connection['adapter']);

			$manager = new ConnectionManagerSingle();
			$manager->setConfiguration($connection);

			$manager->setName($name);
			$serviceContainer->setConnectionManager($name, $manager);
		}
	}
}
