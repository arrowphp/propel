<?php

declare(strict_types=1);

namespace Arrow\Propel;

use Propel\Runtime\Propel;
use Propel\Runtime\Connection\ConnectionInterface;

trait ExecSQL {


	protected function fetchAll(
		string $sql,
		?array $bind = [],
		?ConnectionInterface $conn = null
	): array {
		$stmt = $this->execSQL($sql, $bind, $conn);
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	protected function fetchOne(
		string $sql,
		?array $bind = [],
		?ConnectionInterface $conn = null
	): ?array {
		$stmt = $this->execSQL($sql, $bind, $conn);
		return $stmt->fetch(\PDO::FETCH_ASSOC) ?: null;
	}

	protected function fetchAllIntoModel(
		$class,
		string $sql,
		?array $bind = [],
		?ConnectionInterface $conn = null
	): array {
		$stmt = $this->execSQL($sql, $bind, $conn);
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $class);
		return $stmt->fetchAll() ?: [];
	}

	protected function fetchOneIntoModel(
		$class,
		string $sql,
		?array $bind = [],
		?ConnectionInterface $conn = null
	): ?object {
		$stmt = $this->execSQL($sql, $bind, $conn);
		$stmt->setFetchMode(\PDO::FETCH_CLASS, $class);
		return $stmt->fetch() ?: null;
	}

	protected function execSQL(
		string $sql,
		?array $bind = [],
		?ConnectionInterface $conn = null
	): \PDOStatement {
		$conn = $conn ?: Propel::getConnection();

		/* @phan-suppress-next-line PhanTypeMismatchArgument */
		$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		// TODO cached prepared SQL statements?
		$stmt = $conn->prepare($sql);
		$this->bindParams($stmt, $bind);
		$stmt->execute();

		return $stmt;
	}

	private function bindParams(\PDOStatement $stmt, array $bind): void {
		foreach ($bind as $key => $value) {
			$stmt->bindValue(':' . $key, $value, $this->getBindParamType($value));
		}
	}

	private function getBindParamType($value): int {
		// TODO handle array

		if (is_null($value)) {
			return \PDO::PARAM_NULL;
		} elseif (is_bool($value)) {
			return \PDO::PARAM_BOOL;
		} elseif (is_int($value)) {
			return \PDO::PARAM_INT;
		}

		// Default - Fallback
		return \PDO::PARAM_STR;
	}
}
