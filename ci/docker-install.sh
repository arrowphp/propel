#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq \
  && apt-get install -yqq zip git \
  && pecl install ast-1.0.1

docker-php-ext-enable ast

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
