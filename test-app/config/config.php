<?php declare(strict_types=1);

return [
	// Enables verbose logging
	'Debug' => true,
	// Application namespace
	'Namespace' => 'TestApp',

	'Module' => [
		// Class name of the module class given the module {Name} and {Namespace}
		'ClassNamePattern' => '\{Namespace}\{Name}\{Name}Module',
	],

	'Route' => [
		// Controller and action for Route not found
		'NotFound' => ['module.notfound.controller', 'notFound'],
		// Controller and action for Route found, but not allow / permitted
		'NotAllowed' => ['module.notfound.controller', 'notAllowed'],
	],

	'Middleware' => [
		\Arrow\Middleware\ModuleLoader::class,
	],
	'Package' => [
		'arrowphp/cli' => [
			'Path' => [
				'Base' => __DIR__ . '/../../vendor/arrowphp/cli',
				'Self' => __DIR__ . '/../../vendor/arrowphp/arrow',
			],
			'Namespace' => 'Arrow\\CLI',
		],
		'arrowphp/propel' => [
			'Path' => [
				'Base' => __DIR__ . '/../..',
				'Self' => __DIR__ . '/../../vendor/arrowphp/arrow',
			],
			'Namespace' => 'Arrow\\Propel',

			'Propel' => [
				'database' => [
					'connections' => [
						/*
						 * To override the default Propel connection with a custom Propel connection
						 * define the below in your project config.
						 * To get more information on the Propel Connection Config,
						 * see - http://propelorm.org/documentation/10-configuration.html
						 * */
						'default' => [
							'adapter' => null, // eg. pgsql
							'classname' => 'Propel\Runtime\Connection\ConnectionWrapper',
							'dsn' => null, // eg. pgsql:host=127.0.0.1;port=5432;dbname=application
							'user' => null,
							'password' => null,
							'attributes' => [
								// \PDO::ATTR_PERSISTENT => true,
								// PropelPDO::PROPEL_ATTR_CACHE_PREPARES => true,
							],
						],
					],
				],
				'runtime' => [
					'defaultConnection' => 'default',
					'connections' => ['default'],
				],
				'generator' => [
					'defaultConnection' => 'default',
					'connections' => ['default'],
					'namespaceAutoPackage' => false,
				],
				'reverse' => [
					'connection' => 'default',
				],
			],

			'_Environment_' => [
			  'ARROWPHP_PROPEL_DEFAULT_DSN' => 'Propel.database.connections.default.dsn',
			],

			'_Mutator_' => function (&$config) {
			},
		]
	],
];
